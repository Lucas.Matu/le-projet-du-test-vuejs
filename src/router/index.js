import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import People from '../components/people.vue'
import Search from '../components/search.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    props : true,
  },{
    path :'/',
    name : 'People',
    component : People,
    props : true,
  },{
    path:'/caracter/:name/:mass/:eye',
    name : 'Caracter', 
    component: ()=>import('@/views/caracter.vue'),
    props:true
  }
]
Vue.component('People', People)
Vue.component('Search', Search)

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
